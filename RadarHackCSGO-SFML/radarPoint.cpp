#include "radarPoint.h"
radarPoint::radarPoint(short int team)
{
	drawPoint.setOrigin(pointRadius / 2, pointRadius / 2);
	drawPoint.setRadius(pointRadius);
	drawPoint.setOrigin(pointRadius / 2, pointRadius / 2);
	if(abs(team)==1)
		drawPoint.setFillColor(sf::Color::Red);
	else
		drawPoint.setFillColor(sf::Color::Green);
}


radarPoint::~radarPoint()
{
}

sf::Vector2f radarPoint::getRelativePos(sf::Vector2f coordInit, sf::Vector2f objectPosition)
{
	return coordInit - objectPosition;

}

void radarPoint::setPosition(sf::Vector2f position)
{
	drawPoint.setPosition(position);
}

void radarPoint::setPosition(float positionX, float positionY)
{
	drawPoint.setPosition(positionX, positionY);
}