// RadarHackCSGO-SFML.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>

#include "Vector3.h"
#include <SFML\Graphics\CircleShape.hpp>
#include "radarPoint.h"
#include "offsetsManager.h"

#include "IMGUI\imgui.h"
#include "IMGUI\imgui-SFML.h"

#define RPM ReadProcessMemory

#define URotationToRadians( URotation )		( ( URotation ) * ( 3.1415 / 32768.0f ) ) 
#define URotationToDegree( URotation )		( ( URotation ) * ( 360.0f / 65536.0f ) ) 

#define DegreeToURotation( Degree )			( ( Degree ) * ( 65536.0f / 360.0f ) )
#define DegreeToRadian( Degree )			( ( Degree ) * ( 3.1415 / 180.0f ) )

#define RadianToURotation( URotation )		( ( URotation ) * ( 32768.0f / 3.1415 ) ) 
#define RadianToDegree( Radian )			( ( Radian ) * ( 180.0f / 3.1415 ) )

DWORD pID;
HANDLE iHandle;
HWND hWnd;

DWORD cClient;
DWORD clientDllSize;
DWORD cEngine;
DWORD engineDllSize;

DWORD dw_mLocalPlayer;
DWORD dw_mEntityBase;

struct Store
{
	Store();
	~Store();
public:
	int i_Count, iTeam, iHealth, ennemyNumber, alliesNumber, entityPosList, i_Score, i_Rank, lifeState;
	wchar_t m_wcName[65];
};

Store::Store()
{
	//Constructor
};

Store::~Store()
{
};


sf::Vector2f vec3ToVec2f(Vector3 vectorToConvert)
{
	return (sf::Vector2f(vectorToConvert.X, vectorToConvert.Y));
}

struct MemoryManager
{
	template<class type>
	type Read(DWORD dwAdress)
	{
		type val;
		RPM(iHandle, (LPVOID)(dwAdress), &val, sizeof(type), NULL);
		return val;
	}
	template<class type>
	type Write(DWORD dwAdress, type ValueToWrite)
	{
		return WriteProcessMemory(iHandle, (LPVOID)(dwAdress), &valueToWrite, typeof(type), NULL);
	}
}Mem;

DWORD isPlayer(DWORD *entityValue, short int playerToReadPosOf)
{
	//DWORD entityValue;
	int entityTeam;
	int entityHealth;

	*entityValue=Mem.Read<DWORD>(cClient + dw_mEntityBase + (0x10 * playerToReadPosOf));
	DWORD temp = *entityValue;
	entityTeam=Mem.Read<int>(temp + m_iTeamOffset);
	entityHealth=Mem.Read<int>(temp + m_iHealthOffset);
	if (((entityTeam == 3) || (entityTeam == 2)) && (entityHealth > 0))
		return 1;
	else
		return 0;

	//Maybe a better way to do it, dunno... Need to make some other check...

}

Vector3 getMyPlayerView()
{
	DWORD CLocalPlayer;
	Vector3 myPlayerView;
	if(isPlayer(&CLocalPlayer, 0))
		return myPlayerView = Mem.Read<Vector3>(CLocalPlayer + m_vecViewOffset);
}

Vector3 getMyPlayerPos()
{
	DWORD CLocalPlayer;
	Vector3 myPlayerPos;
	if(isPlayer(&CLocalPlayer,0))
			return myPlayerPos = Mem.Read<Vector3>(CLocalPlayer + m_vecOrigin);
}

Vector3 getPlayerPos(short int entityPos)
{
	DWORD entityValue;
	Vector3 playerPos;
	if(isPlayer(&entityValue, entityPos)){
		playerPos = Mem.Read<Vector3>(entityValue+m_vecOrigin);
	}
	return playerPos;
}

wchar_t *convertCharArrayToLPCWSTR(const char* charArray)
{
	wchar_t* wString = new wchar_t[4096];
	MultiByteToWideChar(CP_ACP, 0, charArray, -1, wString, 4096);
	return wString;
}

sf::Vector2f calcRadarCoordinates(sf::Vector2f myPlayerPos, float myPlayerYaw, sf::Vector2f entityPos, sf::Vector2u windowSize)
{
	float cosYaw = cos(URotationToRadians(myPlayerYaw));
	float sinYaw = sin(URotationToRadians(myPlayerYaw));

	float deltaX = entityPos.x - myPlayerPos.x;
	float deltaY = entityPos.y - myPlayerPos.y;

	float locationX = (deltaY * cosYaw - deltaY * sinYaw) / windowSize.x;
	float locationY = (deltaX * cosYaw + deltaX * sinYaw) / windowSize.y;

	return sf::Vector2f(locationX, locationY);
}

sf::Vector2f calcRadarCoordinates(float myPlayerYaw, sf::Vector2f relativePos, sf::Vector2u windowSize)
{
	float cosYaw = cos(URotationToRadians(myPlayerYaw));
	float sinYaw = sin(URotationToRadians(myPlayerYaw));

	float locationX = (relativePos.y * cosYaw - relativePos.y * sinYaw) / windowSize.x;
	float locationY = (relativePos.x * cosYaw + relativePos.x * sinYaw) / windowSize.y;

	return sf::Vector2f(locationX, locationY);
}

int main()
{
	hWnd = FindWindow(0, convertCharArrayToLPCWSTR("Counter-Strike: Global Offensive"));
	std::cout << hWnd << std::endl;
	GetWindowThreadProcessId(hWnd, &pID);
	iHandle = OpenProcess(PROCESS_ALL_ACCESS, 0, pID);
	if (hWnd)
	{
		if (iHandle)
		{
			std::cout << "CSGO.exe found!" << std::endl;
			cEngine = GetModuleBase(pID, convertCharArrayToLPCWSTR("engine.dll"));
			printf("[cEngine] = %#X\n", cEngine);
			cClient = GetModuleBase(pID, convertCharArrayToLPCWSTR("client.dll"));
			printf("[cClient] = %#X\n", cClient);
			clientDllSize = GetModuleSize(pID, convertCharArrayToLPCWSTR("client.dll"));
			printf("[clientDllSize] = %#X\n", clientDllSize);
			engineDllSize = GetModuleSize(pID, convertCharArrayToLPCWSTR("engine.dll"));
			printf("[engineDllSize] = %#X\n", engineDllSize);
			std::cout << std::endl;
			dw_mLocalPlayer = FindLocalPlayer(cClient, clientDllSize);
			dw_mEntityBase = updateEntityList(cClient, clientDllSize);
			//main needed value
			sf::Vector2f point1pos;

			sf::RenderWindow window(sf::VideoMode(640, 480), "CSGO RadarHack External w/ SFML [ALPHA]");

			//To be removed, done for testing purpose only
			ImGui::SFML::Init(window);

			window.setVerticalSyncEnabled(true);
			sf::CircleShape myPlayer;
			myPlayer.setRadius(6.f);
			myPlayer.setFillColor(sf::Color::Blue);
			myPlayer.setOrigin(3.f, 3.f);
			myPlayer.setPosition((float)window.getSize().x / 2, (float)window.getSize().y / 2);
			radarPoint Point1(4);
			//TODO: Find a way to create object dynamically. I mean create object on the air. Seems to be unpossible...
			sf::Clock deltaClock;
			std::cout << window.getSize().x << std::endl << window.getSize().y << std::endl;
			while (window.isOpen()) {
				sf::Event event;
				ImGui::SFML::Update(deltaClock.restart());
				ImGui::Begin("Debug window!", 0, ImGuiWindowFlags_AlwaysAutoResize);
				while (window.pollEvent(event)) {
					if (event.type == sf::Event::Closed) {
						ImGui::SFML::ProcessEvent(event);
						window.close();
					}
				}

				ImGui::Button("Drag Me");
				if (ImGui::IsItemActive())
				{
					// Draw a line between the button and the mouse cursor
					ImDrawList* draw_list = ImGui::GetWindowDrawList();
					draw_list->PushClipRectFullScreen();
					draw_list->AddLine(ImGui::CalcItemRectClosestPoint(ImGui::GetIO().MousePos, true, -2.0f), ImGui::GetIO().MousePos, ImColor(ImGui::GetStyle().Colors[ImGuiCol_Button]), 4.0f);
					draw_list->PopClipRect();
					ImVec2 value_with_lock_threshold = ImGui::GetMouseDragDelta(0);
					point1pos = value_with_lock_threshold;
				}
				int playerNum;
				ImGui::SliderInt("PlayerNum", &playerNum, 1, 32);
				Vector3 anotherPlayer = getPlayerPos(playerNum);
				ImGui::Text("point1 in-game pos: {%.3f, %.3f, %.3f}", anotherPlayer.X, anotherPlayer.Y, anotherPlayer.Z);

				Vector3 myPlayerPos = getMyPlayerPos();
				sf::Vector2f point1RelatPos = Point1.getRelativePos(vec3ToVec2f(myPlayerPos), vec3ToVec2f(anotherPlayer));
				ImGui::Text("point1RelatPos: {%.3f, %.3f}", point1RelatPos.x, point1RelatPos.y);
				point1RelatPos = calcRadarCoordinates(getMyPlayerView().Y, point1RelatPos, window.getSize());
				point1RelatPos = point1RelatPos + myPlayer.getPosition();
				Point1.setPosition(point1RelatPos);
				ImGui::Text("myPlayerPos: {%.3f, %.3f, %.3f}", myPlayerPos.X, myPlayerPos.Y, myPlayerPos.Z+64);
				window.clear();
				ImGui::End(); // end window
				ImGui::Render();
				window.draw(myPlayer);
				window.draw(Point1.drawPoint);
				window.display();
			}
		}
		}
		ImGui::SFML::Shutdown();
}