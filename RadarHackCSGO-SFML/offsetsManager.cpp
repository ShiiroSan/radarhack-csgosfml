#include "stdafx.h"
#include "offsetsManager.h"

#define RPM ReadProcessMemory
extern HANDLE iHandle;

DWORD GetModuleSize(const DWORD dwProcessId, LPCWSTR szModuleName)
{
	HANDLE hSnap;
	MODULEENTRY32 xModule;
	hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessId);
	xModule.dwSize = sizeof(MODULEENTRY32);
	if (Module32First(hSnap, &xModule))
	{
		while (Module32Next(hSnap, &xModule))
		{
			if (lstrcmpi(xModule.szModule, szModuleName)==0)
			{
				CloseHandle(hSnap);
				return (DWORD)xModule.modBaseSize;
			}
		}
	}
	CloseHandle(hSnap);
	return 0;
}

DWORD GetModuleBase(const DWORD dwProcessId, LPCWSTR szModuleName)
{
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessId);
	if (!hSnap)
	{
		return 0;
	}
	MODULEENTRY32 me;
	me.dwSize = sizeof(MODULEENTRY32);
	DWORD dwReturn = 0;
	if (Module32First(hSnap, &me))
	{
		while (Module32Next(hSnap, &me))
		{
			if (lstrcmpi(me.szModule, szModuleName) == 0)
			{
				dwReturn = (DWORD)me.modBaseAddr;
				break;
			}
		}
	}
	CloseHandle(hSnap);
	return dwReturn;
}

bool DataCompare(BYTE* data, BYTE* sign, char* mask)
{
	for (; *mask; mask++, sign++, data++)
	{
		if (*mask == 'x' && *data != *sign)
		{
			return false;
		}
	}
	return true;
}

DWORD FindSignature(DWORD base, DWORD size, BYTE* sign, char* mask)
{
	MEMORY_BASIC_INFORMATION mbi = { 0 };
	DWORD offset = 0;
	while (offset < size)
	{
		VirtualQueryEx(iHandle, (LPCVOID)(base + offset), &mbi, sizeof(MEMORY_BASIC_INFORMATION));
		if (mbi.State != MEM_FREE)
		{
			BYTE* buffer = new BYTE[mbi.RegionSize];
			ReadProcessMemory(iHandle, mbi.BaseAddress, buffer, mbi.RegionSize, NULL);
			for (unsigned int i = 0; i < mbi.RegionSize; i++)
			{
				if (DataCompare(buffer + i, sign, mask))
				{
					delete[] buffer;
					return (DWORD)mbi.BaseAddress + i;
				}
			}

			delete[] buffer;
		}
		offset += mbi.RegionSize;
	}
	return 0;
}

DWORD FindLocalPlayer(DWORD clientDll, DWORD clientDllSize)
{
	DWORD start, p1, LocPlayer;
	start = FindSignature(clientDll, clientDllSize, (PBYTE)"\xA3\x00\x00\x00\x00\xC7\x05\x00\x00\x00\x00\x00\x00\x00\x00\xE8\x00\x00\x00\x00\x59\xC3\x6A\x00", "x????xx????????x????xxxx");
	/*RPM(iHandle, (void*)(start+3), &p1, sizeof(DWORD),NULL);
	RPM(iHandle, (void*)(start+18), &p2, sizeof(DWORD),NULL);*/
	RPM(iHandle, (void*)(start + 1), &p1, sizeof(DWORD), NULL);
	LocPlayer = ((p1)+0x2C) - clientDll;
	printf("[Local Player Offsets] = %#X\n", LocPlayer);
	return LocPlayer;
}

DWORD updateEntityList(DWORD clientDll, DWORD clientDllSize)
{
	DWORD start, p1, EntList;
	start = FindSignature(clientDll, clientDllSize, (PBYTE)"\xBF\x00\x00\x00\x00\xBB\x01\x00\x00\x00", "x????xxxxx");
	/*RPM(iHandle, (void*)(start+1), &p1, sizeof(DWORD),NULL);
	RPM(iHandle, (void*)(start+7), &p2, sizeof(BYTE),NULL);*/
	RPM(iHandle, (void*)(start + 1), &p1, sizeof(DWORD), NULL);
	EntList = (p1)-clientDll;
	printf("[Entity List Offsets] = %#X\n", EntList);
	return EntList;
}