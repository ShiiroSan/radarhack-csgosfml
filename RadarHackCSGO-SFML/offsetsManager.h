#pragma once

#include <Windows.h>
#include <TlHelp32.h>

const int m_iHealthOffset=0xFC;
const int m_iTeamOffset=0xF0;
const int m_vecOrigin = 0x134;
const int m_vecViewOffset = 0x104;

DWORD GetModuleSize(const DWORD dwProcessId, LPCWSTR szModuleName);
DWORD GetModuleBase(const DWORD dwProcessId, LPCWSTR szModuleName);
DWORD FindSignature(DWORD base, DWORD size, BYTE* sign, char* mask);
DWORD FindLocalPlayer(DWORD clientDll, DWORD clientDllSize);
DWORD updateEntityList(DWORD clientDll, DWORD clientDllSize);