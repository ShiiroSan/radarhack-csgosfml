#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

class radarPoint
{
public:
	radarPoint(short int team);
	sf::CircleShape drawPoint;
	~radarPoint();
	sf::Vector2f getRelativePos(sf::Vector2f coordInit, sf::Vector2f objectPosition);
	void setPosition(sf::Vector2f position);
	void setPosition(float positionX, float positionY);
private:
	float pointRadius = 5.f;
};

